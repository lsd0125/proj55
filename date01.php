<?php
date_default_timezone_set('Asia/Taipei');

header('Content-Type: text/plain');

$today = date("Y-m-d H:i:s");

echo "$today \n";
echo time(). " \n";
echo date("Y-m-d", time()+30*24*60*60). "\n";

echo strtotime('2019-8-20'). "\n";

echo (strtotime('2019-8-20')-time())/(24*60*60);
