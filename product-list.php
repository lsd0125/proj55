<?php require __DIR__. '/__db_connect.php';

$page_name = 'product-list';

$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁
$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0; // 用戶要看哪個分類
$per_page = 4;

// 用來產生 query string
$my_qs = [
    'page' => $page,
    'cate' => $cate,
];

// 取得分類資料
// $c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0 ORDER BY `sequence`"; // 排序條件
$c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0";
$cates = $pdo->query($c_sql)->fetchAll(PDO::FETCH_ASSOC);

$where = " WHERE 1 ";

if(! empty($cate)){
    $where .= " AND `category_sid`=$cate ";
}


// 取得總筆數
$t_sql = "SELECT COUNT(1) FROM `products` $where ";
$totalRows = $pdo->query($t_sql)->fetch(PDO::FETCH_NUM)[0];

$totalPages = ceil($totalRows/$per_page); // 總頁數

// 取得產品資料
$p_sql = sprintf("SELECT * FROM `products` $where LIMIT %s, %s ", ($page-1)*$per_page, $per_page );
$stmt = $pdo->query($p_sql);

$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);


//echo json_encode($cates, JSON_UNESCAPED_UNICODE);
//
//
//exit;

?>

<?php include __DIR__. '/__html_head.php' ?>
<?php include __DIR__. '/__navbar.php' ?>

<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="btn-group-vertical">
                <a class="btn btn-<?= $cate==0 ? '' : 'outline-' ?>primary" href="?cate=0">所有產品</a>
                <?php foreach($cates as $c): ?>
                    <a class="btn btn-<?= $cate==$c['sid'] ? '' : 'outline-' ?>primary" href="?cate=<?= $c['sid'] ?>">
                        <?= $c['name'] ?>
                    </a>
                <?php endforeach; ?>

            </div>

        </div>
        <div class="col-lg-9">
            <div class="row">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <?php for($i=1; $i<=$totalPages; $i++):
                            $my_qs['page'] = $i;
                            ?>
                        <li class="page-item <?= $page==$i ? 'active' : '' ?>">
                            <a class="page-link" href="?<?= http_build_query($my_qs) ?>"><?= $i ?></a>
                        </li>
                        <?php endfor; ?>
                    </ul>
                </nav>
            </div>
            <div class="row">
                <?php foreach($rows as $r): ?>
                <div class="col-lg-3 p-item" data-sid="<?= $r['sid'] ?>">
                    <div class="card">
                        <img src="./imgs/small/<?= $r['book_id'] ?>.jpg" class="card-img-top">
                        <div class="card-body">
                            <p><?= $r['bookname'] ?></p>
                            <p><i class="fas fa-dollar-sign"></i> <?= $r['price'] ?>
                                <button type="button" class="btn btn-primary buy-btn">buy</button>
                            </p>
                            <p>
                                <select class="form-control quantity">
                                    <?php for($i=1; $i<=10; $i++): ?>
                                        <option value="<?= $i ?>"><?= $i ?></option>
                                    <?php endfor; ?>
                                </select>

                            </p>


                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

            </div>

        </div>
    </div>




</div>
<script>
    var buy_btn = $('.buy-btn');
    buy_btn.click(function(){
        var p_item = $(this).closest('.p-item');
        var sid = p_item.attr('data-sid');
        var qty = p_item.find('.quantity').val();
        console.log({sid:sid, qty:qty});

        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            calcQty(data);
            // alert('感謝加入購物車');

        }, 'json');


    });



</script>
<?php include __DIR__. '/__html_foot.php' ?>


