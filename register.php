<?php
require __DIR__. '/__db_connect.php';

$page_name = 'register';

?>
<?php include __DIR__. '/__html_head.php' ?>
<style>
    small{
        color: red;
    }
</style>
<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">

        <div class="col-lg-6">
            <div id="alertInfo" class="alert alert-primary" role="alert" style="display: none;">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">會員註冊</h5>
                    <form id="myform" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <label for="email">** 電子郵箱 (帳號)</label>
                            <input type="text" class="form-control" id="email" name="email">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="nickname">** 暱稱</label>
                            <input type="text" class="form-control" id="nickname" name="nickname">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">** 密碼</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password_check">** 密碼確認</label>
                            <input type="password" class="form-control" id="password_check" name="password_check">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" placeholder="YYYY-MM-DD">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" id="address" name="address" rows="5"></textarea>
                        </div>
                        <button id="submitBtn" type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var alertInfo = $('#alertInfo');
    var submitBtn = $('#submitBtn');
    var $nickname = $('#nickname');
    var $email = $('#email');
    var $password = $('#password');
    var $password_check = $('#password_check');
    var fields = [$email, $nickname, $password, $password_check];

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function checkForm() {
        // 先回復到原來的狀態
        fields.forEach(function(val){
            val.next().text('');
        });
        alertInfo.hide();
        submitBtn.hide();

        var isPass = true; // 表單是否有通過檢查

        if(! validateEmail($email.val())) {
            isPass = false;
            $email.next().text('請輸入正確的 Email 格式');
        }

        if($nickname.val().length < 2) {
            isPass = false;
            $nickname.next().text('請輸入兩個以上的字元');
        }

        if($password.val().length < 6) {
            isPass = false;
            $password.next().text('請輸入六個以上的字元');
        }

        if($password.val() !== $password_check.val()) {
            isPass = false;
            $password_check.next().text('密碼和密碼確認欄裡的值不同');
        }


        if(isPass){

            $.post('register-api.php', $('#myform').serialize(), function(data){
                console.log(data);

                // alert(data.info);

                if(data.success){
                    //location.href = 'data_list.php';
                    alertInfo.removeClass('alert-danger');
                    alertInfo.addClass('alert-success');
                } else {
                    alertInfo.removeClass('alert-success');
                    alertInfo.addClass('alert-danger');
                    submitBtn.show();
                }
                alertInfo.text(data.info);
                alertInfo.show();

            }, 'json');
        }
        return false;
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>
