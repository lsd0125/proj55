<?php require __DIR__. '/__db_connect.php';

$page_name = 'product-list2';
/*
$page = isset($_GET['page']) ? intval($_GET['page']) : 1; // 用戶要看第幾頁
$cate = isset($_GET['cate']) ? intval($_GET['cate']) : 0; // 用戶要看哪個分類
$per_page = 4;

// 用來產生 query string
$my_qs = [
    'page' => $page,
    'cate' => $cate,
];
*/
// 取得分類資料
// $c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0 ORDER BY `sequence`"; // 排序條件
$c_sql = "SELECT * FROM `categories` WHERE `parent_sid`=0";
$cates = $pdo->query($c_sql)->fetchAll(PDO::FETCH_ASSOC);


?>
<?php include __DIR__. '/__html_head.php' ?>
<?php include __DIR__. '/__navbar.php' ?>

<div class="container">
    <div class="row">
        <div class="col-lg-3">
            <div class="btn-group-vertical">
                <a class="btn btn-outline-primary" href="javascript:loadData({cate:0,page:1})">所有產品</a>
                <?php foreach($cates as $c): ?>
                    <a class="btn btn-outline-primary" href="javascript:loadData({cate:<?= $c['sid'] ?>,page:1})">
                        <?= $c['name'] ?>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="row">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                    </ul>
                </nav>
            </div>
            <div class="row" id="products-container">
            </div>

        </div>
    </div>

</div>
<script>
    var params = {
        page: 1,
        cate: 0
    };
    var pagination = $('.pagination');
    var products_container = $('#products-container');

    var pagination_item_str = `<li class="page-item <%= page==i ? 'active' : '' %>">
            <a class="page-link" href="javascript:loadData({page:<%= i %>})"><%= i %></a>
        </li>`;
    var pagination_item_fn = _.template(pagination_item_str);

    var p_item_str = `
        <div class="col-lg-3">
            <div class="card">
                <img src="./imgs/small/<%= book_id %>.jpg" class="card-img-top">
                <div class="card-body">
                    <p><%= bookname %></p>
                    <p><i class="fas fa-dollar-sign"></i> <%= price %>
                        <button type="button" class="btn btn-primary buy-btn">buy</button>
                    </p>
                    <p>
                        <select class="form-control quantity">
                                <option value="1">1</option>
                        </select>
                    </p>
                </div>
            </div>
        </div>`;
    var p_item_fn = _.template(p_item_str);

    function build_pagination(data){
        pagination.html(''); // 清空內容
        var i;
        var page = data.page;
        var totalPages = data.totalPages;

        for(i=1; i<=totalPages; i++){
            pagination.append( pagination_item_fn({i:i, page:page}) );
        }

    }

    function build_products(data){
        var i, obj;
        products_container.html(''); // 清空內容

        for(i=0; i<data.rows.length; i++){
            obj = data.rows[i];

            products_container.append(p_item_fn(obj));
        }
    }

    function loadData(obj){
        if(obj.page!==undefined){
            params.page = obj.page;
        }
        if(obj.cate!==undefined){
            params.cate = obj.cate;
        }

        $.get('product-list2-api.php', params, function(data){
            build_pagination(data);
            build_products(data);
        }, 'json');
    }

    loadData({});



</script>
<?php include __DIR__. '/__html_foot.php' ?>


