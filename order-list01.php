<?php require __DIR__. '/__db_connect.php';

$one_year_ago = date("Y-m-d", time()-365*24*60*60);

$o_sql = "SELECT * FROM `orders` WHERE `order_date`>? AND `member_sid`=? ";
$o_stmt = $pdo->prepare($o_sql);

$o_stmt->execute([
    $one_year_ago,
    $_SESSION['loginUser']['sid'],
]);

$o_rows = $o_stmt->fetchAll(PDO::FETCH_ASSOC);

$o_sids = [];
foreach($o_rows as $r){
    $o_sids[] = $r['sid'];
}

$od_sql = sprintf("SELECT * FROM `order_details` WHERE `order_sid` IN (%s)",
    implode(',', $o_sids));
$od_stmt = $pdo->query($od_sql);

$od_rows = $od_stmt->fetchAll(PDO::FETCH_ASSOC);


?>
<?php include __DIR__. '/__html_head.php' ?>
<?php include __DIR__. '/__navbar.php' ?>

<div class="container">

    <h2>order-list</h2>
<pre>
    <?php
    print_r($o_rows);
    print_r($od_rows);


    ?>
</pre>

</div>

<?php include __DIR__. '/__html_foot.php' ?>


