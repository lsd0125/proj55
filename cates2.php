<?php require __DIR__. '/__db_connect.php';

$c_sql = "SELECT * FROM `categories` ORDER BY `parent_sid` ";
$cates = $pdo->query($c_sql)->fetchAll(PDO::FETCH_ASSOC);

$menu_data = [];

// 先拿到第一層的選單資料
foreach($cates as $c){
    if($c['parent_sid']==0){
        //$c['nodes'] = []; // 放子節點的地方
        $menu_data[] = $c;
    }
}

// 把第二層的項目放到第一層底下
foreach($menu_data as $km => $m) {
    // echo json_encode($m, JSON_UNESCAPED_UNICODE). '<br>';
    foreach($cates as $c){
        // printf("c: %s, %s,= %s<br>", $c['parent_sid'], $m['sid'], $c['parent_sid'] == $m['sid']);
        if($c['parent_sid'] == $m['sid']){
            // $m['nodes'][] = $c; // 錯誤的寫法
            $menu_data[$km]['nodes'][] = $c;
        }
    }
}


//echo json_encode($menu_data, JSON_UNESCAPED_UNICODE);
//exit;


?>
<?php include __DIR__. '/__html_head.php' ?>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <?php foreach($menu_data as $m1): ?>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown">
                    <?= $m1['name'] ?>
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <?php foreach($m1['nodes'] as $m2): ?>
                    <a class="dropdown-item" href="?cate=<?= $m2['sid'] ?>"><?= $m2['name'] ?></a>
                    <?php endforeach; ?>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</nav>

<div class="container">



</div>

<?php include __DIR__. '/__html_foot.php' ?>


