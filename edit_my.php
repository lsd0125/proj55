<?php
require __DIR__. '/__db_connect.php';

$page_name = 'edit_my';

$sql = "SELECT * FROM `members` WHERE `sid`=". intval($_SESSION['loginUser']['sid']);
$row = $pdo->query($sql)->fetch(PDO::FETCH_ASSOC);

if(empty($row)){
    header('Location: ./');
    exit;
}

?>
<?php include __DIR__. '/__html_head.php' ?>
<style>
    small{
        color: red;
    }
</style>
<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">

        <div class="col-lg-6">
            <div id="alertInfo" class="alert alert-primary" role="alert" style="display: none;">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">編輯個人資料</h5>
                    <form id="myform" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <input type="hidden" name="sid" value="<?= $row['sid'] ?>">
                            <label for="email">電子郵箱 (帳號)</label>
                            <input type="text" class="form-control" id="email" name="email" value="<?= htmlentities($row['email']) ?>" disabled>
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="nickname">** 暱稱</label>
                            <input type="text" class="form-control" id="nickname" name="nickname" value="<?= htmlentities($row['nickname']) ?>">
                            <small class="form-text"></small>
                        </div>

                        <div class="form-group">
                            <label for="mobile">手機</label>
                            <input type="text" class="form-control" id="mobile" name="mobile" value="<?= htmlentities($row['mobile']) ?>">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="birthday">生日</label>
                            <input type="text" class="form-control" id="birthday" name="birthday" value="<?= htmlentities($row['birthday']) ?>">
                        </div>
                        <div class="form-group">
                            <label for="address">地址</label>
                            <textarea class="form-control" id="address" name="address" rows="5"><?= htmlentities($row['address']) ?></textarea>
                        </div>

                        <div class="form-group">
                            <label for="password">** 請輸入原密碼 (安全機制)</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <small class="form-text"></small>
                        </div>
                        <button id="submitBtn" type="submit" class="btn btn-primary">修改</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var alertInfo = $('#alertInfo');
    var submitBtn = $('#submitBtn');
    var $nickname = $('#nickname');

    var $password = $('#password');

    var fields = [$nickname, $password];

    function checkForm() {
        // 先回復到原來的狀態
        fields.forEach(function(val){
            val.next().text('');
        });
        alertInfo.hide();
        submitBtn.hide();

        var isPass = true; // 表單是否有通過檢查

        if($nickname.val().length < 2) {
            isPass = false;
            $nickname.next().text('請輸入兩個以上的字元');
        }

        if($password.val().length < 6) {
            isPass = false;
            $password.next().text('請輸入六個以上的字元');
        }

        if(isPass){
            $.post('edit-my-api.php', $('#myform').serialize(), function(data){
                console.log(data);

                if(data.success){
                    //location.href = 'data_list.php';
                    alertInfo.removeClass('alert-danger');
                    alertInfo.addClass('alert-success');

                    $('#my_nickname').text($nickname.val());
                } else {
                    alertInfo.removeClass('alert-success');
                    alertInfo.addClass('alert-danger');
                    submitBtn.show();
                }
                alertInfo.text(data.info);
                alertInfo.show();

            }, 'json');
        } else {
            submitBtn.show();
        }
        return false;
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>
