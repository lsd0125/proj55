<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand" href="./">Home</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?= $page_name=='product-list' ? 'active' : '' ?>">
                    <a class="nav-link" href="product-list.php">產品列表</a>
                </li>
                <li class="nav-item <?= $page_name=='product-list2' ? 'active' : '' ?>">
                    <a class="nav-link" href="product-list2.php">產品列表 (ajax)</a>
                </li>
                <li class="nav-item <?= $page_name=='cart' ? 'active' : '' ?>">
                    <a class="nav-link" href="cart.php">購物車
                        <span class="badge badge-pill badge-primary cart-qty"></span>
                    </a>

                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="form_test.php">form test</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="cates2.php">二層選單測試</a>
                </li>
            </ul>
            <?php if(isset($_SESSION['loginUser'])): ?>
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" id="my_nickname"><?= $_SESSION['loginUser']['nickname'] ?></a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="edit_my.php">編輯個人資料</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="logout.php">登出</a>
                </li>
            </ul>
            <?php else: ?>
            <ul class="navbar-nav">
                <li class="nav-item ">
                    <a class="nav-link" href="login.php">登入</a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" href="register.php">註冊</a>
                </li>
            </ul>
            <?php endif ?>

        </div>
    </div>
</nav>
<style>
    .nav-item.active {
        background-color: #7abaff;
    }
</style>
<script>
    function calcQty(cart_data){
        var sum = 0;
        for(let s in cart_data){
            sum += cart_data[s];
        }
        $('.cart-qty').text(sum);
    }


    $.get('add_to_cart.php', function(data){
        calcQty(data);
    }, 'json');
</script>