<?php require __DIR__. '/__db_connect.php';

if(! empty($_SESSION['cart'])){
    $keys = array_keys($_SESSION['cart']);

    $sql = sprintf("SELECT * FROM `products` WHERE `sid` IN (%s)",
        implode(',', $keys));
    $stmt = $pdo->query($sql);
    $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

    $dict = array();
    foreach($rows as $r){
        $dict[$r['sid']] = $r;
    }
//    header('Content-Type: text/plain');
//    print_r($dict);
//    print_r($keys);
//    exit;
} else {
    header('Location: product-list.php');
    exit;
}







?>

<?php include __DIR__. '/__html_head.php' ?>
<?php include __DIR__. '/__navbar.php' ?>

<div class="container">
    <table class="table table-striped table-bordered">
        <thead>
        <tr>
            <th scope="col"><i class="fas fa-trash"></i></th>
            <th scope="col">封面</th>
            <th scope="col">書名</th>
            <th scope="col">單價</th>
            <th scope="col">數量</th>
            <th scope="col">小計</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach($keys as $k):
            $r = $dict[$k];
            ?>
        <tr class="p-item" data-sid="<?= $r['sid'] ?>">
            <td>
                <button class="btn btn-danger remove-btn">
                    <i class="fas fa-trash"></i>
                </button>
            </td>
            <td><img src="./imgs/small/<?= $r['book_id'] ?>.jpg" alt=""></td>
            <td><?= $r['bookname'] ?></td>
            <td class="price" data-price="<?= $r['price'] ?>"></td>
            <td>
                <select class="qty" data-qty="<?= $_SESSION['cart'][$k] ?>">
                    <?php for($i=1; $i<=20; $i++): ?>
                    <option value="<?= $i ?>"><?= $i ?></option>
                    <?php endfor; ?>
                </select>
            </td>
            <td class="subtotal"></td>
        </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

    <div class="alert alert-info" role="alert">總計: <span id="total_price"></span></div>

    <?php if(isset($_SESSION['loginUser'])): ?>
        <a class="btn btn-success" href="buy.php">結帳</a>
    <?php else: ?>
        <a class="btn btn-danger" href="login.php">請登入後再結帳</a>
    <?php endif ?>
</div>
<script>
    var p_items = $('.p-item');
    var remove_btns = $('.remove-btn');

    var dallorCommas = function(n){
        return n.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
    };

    p_items.each(function(){
        var price = $(this).find('.price').attr('data-price');
        var qty = $(this).find('.qty').attr('data-qty');
        $(this).find('.subtotal').text('$ '+ dallorCommas(price*qty));
        $(this).find('.price').text('$ '+ dallorCommas(price));

        // select element
        $(this).find('.qty').val(qty);
    });

    remove_btns.click(function(){
        var tr = $(this).closest('.p-item');
        var sid = tr.attr('data-sid');

        $.get('add_to_cart.php', {sid:sid}, function(data){
            calcQty(data);
            tr.remove();
            calcTotalPrice();
        }, 'json');
    });

    $('select.qty').change(function(){
        var tr = $(this).closest('.p-item');
        var sid = tr.attr('data-sid');
        var price = tr.find('.price').attr('data-price');
        var qty = $(this).val();
        $.get('add_to_cart.php', {sid:sid, qty:qty}, function(data){
            calcQty(data);
            tr.find('.subtotal').text('$ ' + dallorCommas(price*qty));
            calcTotalPrice();
        }, 'json');
    });

    function calcTotalPrice() {
        var t = 0;
        $('.p-item').each(function(){
            var price = $(this).find('.price').attr('data-price');
            var qty = $(this).find('.qty').val();

            t += price*qty;
        });

        $('#total_price').text( '$ ' + dallorCommas(t));
    }
    calcTotalPrice();
</script>
<?php include __DIR__. '/__html_foot.php' ?>


