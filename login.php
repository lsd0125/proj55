<?php
require __DIR__. '/__db_connect.php';

$page_name = 'login';

if(empty($_SERVER['HTTP_REFERER'])){
    $backToUrl = './';
} else {
    $backToUrl = $_SERVER['HTTP_REFERER'];
}

?>
<?php include __DIR__. '/__html_head.php' ?>
<style>
    small{
        color: red;
    }
</style>
<?php include '__navbar.php' ?>
<div class="container">

    <div class="row" style="margin-top: 2rem;">

        <div class="col-lg-6">
            <div id="alertInfo" class="alert alert-primary" role="alert" style="display: none;">
            </div>
            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">會員登入</h5>
                    <form id="myform" method="post" onsubmit="return checkForm()">
                        <div class="form-group">
                            <label for="email">** 電子郵箱 (帳號)</label>
                            <input type="text" class="form-control" id="email" name="email">
                            <small class="form-text"></small>
                        </div>
                        <div class="form-group">
                            <label for="password">** 密碼</label>
                            <input type="password" class="form-control" id="password" name="password">
                            <small class="form-text"></small>
                        </div>
                        <button id="submitBtn" type="submit" class="btn btn-primary">登入</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var backToUrl = '<?= $backToUrl ?>';
    var alertInfo = $('#alertInfo');
    var submitBtn = $('#submitBtn');

    var $email = $('#email');
    var $password = $('#password');

    var fields = [$email, $password];

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }

    function checkForm() {
        // 先回復到原來的狀態
        fields.forEach(function(val){
            val.next().text('');
        });
        alertInfo.hide();
        submitBtn.hide();

        var isPass = true; // 表單是否有通過檢查

        if(! validateEmail($email.val())) {
            isPass = false;
            $email.next().text('請輸入正確的 Email 格式');
        }

        if($password.val().length < 6) {
            isPass = false;
            $password.next().text('請輸入六個以上的字元');
        }

        if(isPass){

            $.post('login-api.php', $('#myform').serialize(), function(data){
                console.log(data);

                if(data.success){
                    //location.href = 'data_list.php';
                    alertInfo.removeClass('alert-danger');
                    alertInfo.addClass('alert-success');

                    setTimeout(function (){
                        location.href = backToUrl;
                    }, 2000);
                } else {
                    alertInfo.removeClass('alert-success');
                    alertInfo.addClass('alert-danger');
                    submitBtn.show();
                }
                alertInfo.text(data.info);
                alertInfo.show();

            }, 'json');
        }
        return false;
    }
</script>
<?php include __DIR__. '/__html_foot.php' ?>
